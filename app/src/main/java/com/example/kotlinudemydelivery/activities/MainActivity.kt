package com.example.kotlinudemydelivery.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.example.kotlinudemydelivery.R

class MainActivity : AppCompatActivity() {

    var imageViewGoToRegister: ImageView ? = null;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        imageViewGoToRegister = findViewById( R.id.imageview_go_to_register )
        imageViewGoToRegister?.setOnClickListener{ goToRegister() }

    }

    private fun goToRegister(){
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity( intent )
    }
}